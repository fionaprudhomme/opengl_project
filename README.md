# READ ME

## Our Project 

In this git you will find an OpenGL project which is an 3D city. This city is created with differents modules asked by our teacher. 

We are two student of an ingineer school : ENSG (Ecole Nationale des sciences géographique).

Students : Fiona Prud'homme (fiona.prud-homme@ensg.eu) and Etienne Le Bihan (etienne.le-bihan@ensg.eu).

## Modules Implemented

1. Dynamic light
    * A light source that moves in the "sky" (here the sun)
    * A fixed light during the "night" (preferably coming from a street lamp!)
    * Play with the colors of light to generate twilight lighting
2. Realistic camera
    * Modify theCameraclass to constrain the movement parallel to the ground, asif we were "walking" on the ground
    * Add a visualization mode where the camera is situated in the air and pointingpermanently to the ground (here you can use the space bar to see that)
    * allow changing the viewing mode by pressing a key on the keyboard (here the space bar)
3. Random world generation
    * Make sure that the world is generated randomly at the start of the application:the different objects are randomly placed in the space, butconsistent(nolevitation or collision between objects)
4. Adding planned moves
    * Move one or more vehicles according to a predefined or automaticpattern (here a Kangaroo)
    * move the camera independently, like an animated movie (using the space bar)

## Installation

After downloading/cloning the repository, open a terminal in the folder and type :

`cmake .`

then

`make`

then

`./main`
