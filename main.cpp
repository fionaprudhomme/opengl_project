#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <map>

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>  

// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>
//GLUT
#include <GL/glut.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Shader loading class
#include "include/Shader.h"
// Camera loading class
#include "include/Camera.h"
// Model loading class
#include "include/Model.h"


// Other Libs
#include <SOIL.h>

using namespace std;

// Dimension of the window
const GLuint WIDTH = 800, HEIGHT = 600;


//Changes of the light of the day because of the rotation of the sun 

GLfloat ANGLE_DUSK(3*M_PI/5);
GLfloat ANGLE_DOWN(4*M_PI/3);

//Duration of the cycle night/day. 240 = 2 mins of day + 2 mins of night
GLint CYCLE(240);

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
//void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);
unsigned int loadTexture(const char *path);

// settings
const unsigned int SCR_WIDTH = 1280;
const unsigned int SCR_HEIGHT = 720;




// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;




static void Timer(int value){
    //angle += 0.1;
    glutPostRedisplay();
    // 100 milliseconds
    glutTimerFunc(10000, Timer, 0);
}


// Main function, it is here that we initialize what is relative to the context
// of OpenGL and that we launch the main loop
int main(int argc, char *argv[])
{


    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // Initialization of GLFW
    glfwInit();
    glutInit(&argc,argv);



    // Specify the OpenGL version to be used (3.3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    // Disable the deprecated functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // Necesary for Mac OS
    #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
    // Prevent the change of dimension of the window
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // Create the application window
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Etienne&Fiona", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }


    // Associate the created window with the OpenGL context
    glfwMakeContextCurrent(window);
    // Associate the callback with the pressure of a key on the keyboard
    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
   // glfwSetScrollCallback(window, scroll_callback);

    // Variable global that allows to ask the GLEW library to find the modern
    // functions of OpenGL
    glewExperimental = GL_TRUE;
    // Initializing GLEW to retrieve pointers from OpenGL functions
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }

    // Transform the window dimensions to the dimensions of OpenGL (between -1 and 1)
    int width, height;
    // Recover the dimensions of the window created above
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);

    // Allow to test the depth information
    glEnable(GL_DEPTH_TEST);

    // Build and compile our vertex and fragment shaders
    Shader shaders("shaders/default.vertexshader","shaders/default.fragmentshader");
    // Declare the positions and uv coordinates in the same buffer
    GLfloat vertices[] = {
        // Positions            // Colors         // UV
        -500.0f, 0.0f,  500.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1250.0f, // Top right point
        -500.0f, 0.0f, -500.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, // Bottom right point
         500.0f, 0.0f,  500.0f, 0.0f, 1.0f, 0.0f, 1250.0f, 1250.0f, // Bottom left point
         500.0f, 0.0f, -500.0f, 0.0f, 1.0f, 0.0f, 1250.0f, 0.0f, // Top left point
    };

    // The table of indices to rebuild our triangles
    GLushort indices[] = {
        0, 1, 2,   // First triangle
        1, 3, 2    // Second triangle
    };

    // Declare the identifiers of our VAO, VBO and EBO
    GLuint VAO, VBO, EBO;
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(1, &VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(1, &VBO);
    // Inform OpenGL to generate one EBO
    glGenBuffers(1, &EBO);

    // Bind the VAO
    glBindVertexArray(VAO);
    // Bind and fill the VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // Bind and fill the EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
        (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // UV coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
        (GLvoid*)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    // second, configure the light's VAO (VBO stays the same; the vertices are the same for the light object which is also a 3D cube)
    unsigned int lightVAO;
    glGenVertexArrays(1, &lightVAO);
    glBindVertexArray(lightVAO);

    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    // EBO is detached (the last one!)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Declare the texture identifier
    GLuint texture;
    // Generate the texture
    glGenTextures(1, &texture);
    // Bind the texture created in the global context of OpenGL
    glBindTexture(GL_TEXTURE_2D, texture);
    // Method of wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    // Filtering method
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


    glDrawArrays(GL_TRIANGLES, 0, 6);



    // Loading the image file using the SOIL lib
    int twidth, theight;
    unsigned char* data = SOIL_load_image("texture/sable.jpg",
        &twidth, &theight, 0, SOIL_LOAD_RGB);
    // Associate the image data with texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight,
        0, GL_RGB, GL_UNSIGNED_BYTE, data);
    // Generate the mipmap
    glGenerateMipmap(GL_TEXTURE_2D);
    // Free the memory
    SOIL_free_image_data(data);
    // Unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);

    // Camera
    Camera camera(glm::vec3(0.0f, 1.0f, 0.0f), window);


    float lastX = (float)SCR_WIDTH  / 2.0;
    float lastY = (float)SCR_HEIGHT / 2.0;
    bool firstMouse = true;


    // IMPORTING THE MODELS 
    
	Model cottage("model/31-village-house/medieval house.obj");
    Model romboo("model/house/Campfire OBJ.obj");

    Model sun("model/Sun_J/13913_Sun_v2_l3.obj");
    
    Model palm("model/Palm_01/Palm_01.obj");
    Model palm2("model/palm2/10446_Palm_Tree_v1_max2010_iteration-2.obj");
    
    Model kangoo("model/kangoo/12271_Kangaroo_v1_L3.obj");
    Model giraf("model/giraf/10021_Giraffe_v04.obj");

    Model lamp("model/lamp/Lamp.obj");
    Model lampN("model/lamp/litLamp/Lamp.obj");

    
    srand(std::time(NULL));

    // random number of houses spawning between 1 and 4
    int nbrHouseHood1 = 1 + rand()% 4;
    
    // randomly take one type of tree or the other for the tree wall
    int typeTree = 1 + (rand()%2);

    // randomly take one type of animal which will be moving
    int typeAnimal = 1 + (rand()%2);

    //std::vector<int> housesT = {thouse1,thouse2,thouse3,thouse4};
    std::vector<int> housesT = {1,1,1,1};
    std::vector<int> lamps = {rand()%2,rand()%2,rand()%2,rand()%2};

    int countTour = 0;

    cout << "type ani :"<< typeAnimal << endl;
    cout << "nbr house in hood 1:" << nbrHouseHood1 << endl;
    cout << "type palm :"<< typeTree << endl;

   // The main loop of the program
    while (!glfwWindowShouldClose(window))
    {

        // Retrieve events and call the corresponding callback functions
        glfwPollEvents();
        camera.Do_Movement();
        //Fonction permettant de changer de mode de caméra, appelée si la barre espace est enfoncée
        camera.Switch_Mode();
        glUniform3f(glGetUniformLocation(shaders.Program, "viewPos"), camera.Position.x, camera.Position.y, camera.Position.z);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shaders.Use();

        // Recover the identifiers of the global variables of the shader
        GLint modelLoc = glGetUniformLocation(shaders.Program, "model");
        GLint viewLoc  = glGetUniformLocation(shaders.Program, "view");
        GLint projLoc  = glGetUniformLocation(shaders.Program, "projection");

        glm::mat4 projection = glm::perspective(45.0f, (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 350.0f);

        // Camera matrix
        glm::mat4 view;
        view = camera.GetViewMatrix();

        // Model matrix (translation, rotation and scale)
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::scale(model,glm::vec3(0.2f,0.2f,0.2f));
        //Sun otation matrix around the Z axis
        glm::vec4 lPos(camera.Position.x, camera.Position.y + 300.0f, camera.Position.z, 1.0f);
        glm::vec3 lColor(1.0f, 1.0f, 1.0f);
        glm::vec3 clearColor;

        GLint lightPos = glGetUniformLocation(shaders.Program, "lightPos");
        GLint lightColor = glGetUniformLocation(shaders.Program, "lightColor");
        GLint lampPos = glGetUniformLocation(shaders.Program, "lampPos");
        GLint lampColor = glGetUniformLocation(shaders.Program, "lampColor");
        GLint ambientStrength = glGetUniformLocation(shaders.Program, "ambientStrength");
        GLint specularStrength = glGetUniformLocation(shaders.Program, "specularStrength");
        GLfloat ambStr(0.05f);
        GLfloat specStr(0.3f);
        
        glm::mat4 rot;
        GLfloat angle = glm::mod(2*M_PI*(glfwGetTime()/CYCLE), 2*M_PI);
        rot = glm::rotate(rot, angle, glm::vec3(0.0f, 0.0f, 1.0f));
        lPos = rot * lPos;

        glm::vec3 lmpColor;

        if (angle > ANGLE_DUSK && angle < ANGLE_DOWN){
            lmpColor = glm::vec3(1.0f, 0.56f, 0.17f);
            lColor = glm::vec3(0.0f, 0.0f, 0.05f);
            clearColor = glm::vec3(0.0f, 0.0f, 0.2f);
            ambStr = 0.05f;

        //
        } else if (angle > M_PI/4 && angle < ANGLE_DUSK){
            lmpColor = glm::vec3(1.0f, 0.56f, 0.17f);
            double x = (ANGLE_DUSK - angle)/(ANGLE_DUSK - M_PI/4);
            lColor = glm::vec3(x * 1.0f, pow(x, 2) * 1.0f, pow(x, 4) * 1.0f + 0.05f);
            clearColor = glm::vec3((-2*pow(x,2) + 2*x) * 1.0f, pow(x, 2) * 0.5f, pow(x, 4) * 1.0f + 0.1f);
            ambStr = x * 0.65f + 0.05f;


        //sunrise
        } else if (angle > ANGLE_DOWN && angle < 5*M_PI/3){
            lmpColor = glm::vec3(1.0f, 0.56f, 0.17f);
            double x = (ANGLE_DOWN - angle)/(ANGLE_DOWN - 5*M_PI/3);
            lColor = glm::vec3(pow(x,4) * 1.0f, pow(x, 2) * 1.0f, x * 1.0f + 0.05f);
            clearColor = glm::vec3((-2*pow(x,2) + 2*x) * 1.0f, pow(x, 2) * 0.5f, pow(x, 4) * 1.0f + 0.1f);
            ambStr = x * 0.65f + 0.05f;


        //Day light
        } else{
            lmpColor = glm::vec3(0.0f, 0.0f, 0.0f);
            lColor = glm::vec3(1.0f, 1.0f, 1.0f);
            clearColor = glm::vec3(0.0f, 0.5f, 1.0f);
            ambStr = 0.9f;
        }

        glClearColor(clearColor.x, clearColor.y, clearColor.z, 1.0f);
        glUniform3f(lightPos, lPos.x, lPos.y, lPos.z);
        glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);
        glUniform1f(ambientStrength, ambStr);
        glUniform1f(specularStrength, specStr);


        glUniform3f(glGetUniformLocation(shaders.Program, "viewPos"), camera.Position.x, camera.Position.y, camera.Position.z);

        
        // Update the global variables of the shader
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));



        // Bind the VAO as a current object in the context of OpenGL
        glBindVertexArray(VAO);
        // Activate texture 0
        glActiveTexture(GL_TEXTURE0);
        // Bind the texture
        glBindTexture(GL_TEXTURE_2D, texture);
        // Associate the texture with the shader
        glUniform1i(glGetUniformLocation(shaders.Program, "modelTexture"), 0);
        // Draw the current object
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);


        // LAMP : the lamp is the light source during the night 
        glm::vec3 lmpPos(0.0f, 1.6f, -4.0f);
        glUniform3f(lampPos, lmpPos.x, lmpPos.y , lmpPos.z);
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(lmpPos.x, 0.0f, lmpPos.z));
        model = glm::scale(model, glm::vec3(1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        if(lmpColor.x != 0.0f){
            glUniform1f(ambientStrength, 0.7f);
            glUniform3f(lightColor, lmpColor.x, lmpColor.y, lmpColor.z);

            lampN.Draw(shaders);
            glUniform1f(ambientStrength, ambStr);
            glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);

        } else{
            lamp.Draw(shaders);
        }


        //SUN
        model = glm::mat4(1.0f);
        model = glm::rotate(model, -angle, glm::vec3(-5000.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.01f));
        model = glm::translate(model,glm::vec3(1.0f,10000.0f,1.0f));

        glUniform1f(ambientStrength, 5.0f);
       // glUniform3f(lightColor, lColor.x + 0.4f, clearColor.y, 0.0f);
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        sun.Draw(shaders);

        glUniform3f(lampColor, lmpColor.x, lmpColor.y , lmpColor.z);
        glUniform1f(ambientStrength, ambStr);
        glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);
       
     
        
        ///// HOOD 1 /////

                // TREE WALL surrounding the hood : can be of two type of palm (random) //
        
                    int tree_i =1;

                    if(typeTree==1){
                        
                        while(tree_i <= 40){
                        model = glm::mat4(1.0f);
                        
                        if(tree_i%4==0){
                            model = glm::translate(model,glm::vec3(-6.0f+tree_i,0.0f,-20.0f));
                            }
                        else if(tree_i%4==1){
                            model = glm::translate(model,glm::vec3(-6.0f,0.0f,-20.0f+tree_i));
                            }
                            else if(tree_i%4==2){
                            model = glm::translate(model,glm::vec3(-6.0f+tree_i,0.0f,-20.0f+40));
                            }
                            else if(tree_i%4==3){
                            model = glm::translate(model,glm::vec3(-6.0f+40,0.0f,-20.0f+tree_i));
                            }
                        
                        model = glm::scale(model, glm::vec3(0.5f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        palm.Draw(shaders);
                        tree_i ++;
                    }}
                    else{ 
                        while(tree_i <= 20){
                        model = glm::mat4(1.0f);
                        
                        if(tree_i%4==0){
                            model = glm::translate(model,glm::vec3(-6.0f+tree_i*2.0,0.0f,-20.0f));
                            }
                        else if(tree_i%4==1){
                            model = glm::translate(model,glm::vec3(-6.0f,0.0f,-20.0f+tree_i*2.0));
                            }
                            else if(tree_i%4==2){
                            model = glm::translate(model,glm::vec3(-6.0f+tree_i*2.0,0.0f,-20.0f+40));
                            }
                            else if(tree_i%4==3){
                            model = glm::translate(model,glm::vec3(-6.0f+40,0.0f,-20.0f+tree_i*2.0));
                            }

                        model = glm::scale(model, glm::vec3(0.01f));
                        GLfloat anglePalm = M_PI/2 ;
                        model = glm::rotate(model, anglePalm, glm::vec3(-1.0f, 0.0f, 0.0f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        palm2.Draw(shaders);
                        tree_i ++;
                    }
                        
                        
                        
                        }



        // HOOD HOUSES : a random number of houses between 1 and 4 is created 
        int Housesi = 1;
        int distanceBetween = 10;



        while  (Housesi <= nbrHouseHood1){
          model = glm::mat4(1.0f);
          
          if (housesT.at(Housesi-1)==1)
          {
              if(Housesi==1){
      
            model = glm::translate(model,glm::vec3(4.0f,0.0f,-10.0f));
          }
          else if (Housesi==2){
          model = glm::translate(model,glm::vec3(4.0f,0.0f,-10.0f+distanceBetween));
    
          }
          else if (Housesi==3){
          model = glm::translate(model,glm::vec3(4.0f+distanceBetween,0.0f,-10.0f+distanceBetween));
         
          
          }
          else if (Housesi==4){
          model = glm::translate(model,glm::vec3(4.0f+distanceBetween,0.0f,-10.0f));
          
          }
          
          model = glm::scale(model, glm::vec3(0.5f));
          glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
          cottage.Draw(shaders);
          }
          
          // the second type of houses (tents)
    /*       else
          {
              if(Housesi==1){
          model = glm::translate(model,glm::vec3(4.0f,-0.01f,-10.0f));          
          }
          else if (Housesi==2){
          model = glm::translate(model,glm::vec3(4.0f,-0.01f,-10.0f+distanceBetween));
          }
          else if (Housesi==3){
          model = glm::translate(model,glm::vec3(4.0f+distanceBetween,-0.01f,-10.0f+distanceBetween));
          }
          else if (Housesi==4){
          model = glm::translate(model,glm::vec3(4.0f+distanceBetween,-0.01f,-10.0f));
          }
          
          model = glm::scale(model, glm::vec3(0.1f));
          glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
          romboo.Draw(shaders);
          }
                */     
          Housesi ++;
         
        }




        // ANIMAL : the animal is moving, following a defined path but the animal is radomly a KANGOOROO OR A GIRAFF //

        if(glfwGetTime()-40.0*(countTour+1)>=0){
                countTour++;
            }
        
        cout << countTour << endl ;

        if(typeAnimal==1){
        model = glm::mat4(1.0f);
        if(10.0f - countTour*40 + glfwGetTime()<20.0f){

            float xpos = 10.0f - countTour*40 + glfwGetTime();
            model = glm::translate(model,glm::vec3(xpos,0.0f,-5.0f));
            }
        else if(-5.0 - (10+40*countTour) +glfwGetTime() < 5.0f) { 
            
            float ypos = -5.0 - (10+40*countTour) +glfwGetTime();
            model = glm::translate(model,glm::vec3(20.0f,0.0f,ypos));
            GLfloat angleKangoo = M_PI/2 ;
            model = glm::rotate(model, angleKangoo, glm::vec3(0.0f, -1.0f, 0.0f));}
        else if(20.0f + (20+countTour*40) - glfwGetTime()>10.0f){
   
            float xpos = 20.0f + (20+countTour*40) - glfwGetTime();
            model = glm::translate(model,glm::vec3(xpos,0.0f,5.0f));
            GLfloat angleKangoo = M_PI ;
            model = glm::rotate(model, angleKangoo, glm::vec3(0.0f, 1.0f, 0.0f));}
        else if(5.0f + (30+countTour*40) - glfwGetTime()>-5.0f){

            float ypos = 5.0f + (30+countTour*40) - glfwGetTime();
            model = glm::translate(model,glm::vec3(10.0f,0.0f,ypos));
            GLfloat angleKangoo = M_PI/2 ;
            model = glm::rotate(model, angleKangoo, glm::vec3(0.0f, 1.0f, 0.0f));
            }
        


        GLfloat angleKangoo = M_PI/2 ;
        model = glm::rotate(model, angleKangoo, glm::vec3(-1.0f, 0.0f, 0.0f));
        model = glm::scale(model, glm::vec3(0.01f));

        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        kangoo.Draw(shaders);
        }
        else{
        model = glm::mat4(1.0f);
        
         
        if(10.0f - countTour*40 + glfwGetTime()<20.0f){

            float xpos = 10.0f - countTour*40 + glfwGetTime();
            model = glm::translate(model,glm::vec3(xpos,0.0f,-5.0f));
            GLfloat angleGiraf = M_PI/2 ;
            model = glm::rotate(model, angleGiraf, glm::vec3(0.0f, -1.0f, 0.0f));}
        else if(-5.0 - (10+40*countTour) +glfwGetTime() < 5.0f) { 
            
            float ypos = -5.0 - (10+40*countTour) +glfwGetTime();
            model = glm::translate(model,glm::vec3(20.0f,0.0f,ypos));
            GLfloat angleGiraf = M_PI ;
            model = glm::rotate(model, angleGiraf, glm::vec3(0.0f, 1.0f, 0.0f));
            }
        else if(20.0f + (20+countTour*40) - glfwGetTime()>10.0f){
   
            float xpos = 20.0f + (20+countTour*40) - glfwGetTime();
            model = glm::translate(model,glm::vec3(xpos,0.0f,5.0f));
            GLfloat angleGiraf = M_PI/2 ;
            model = glm::rotate(model, angleGiraf, glm::vec3(0.0f, 1.0f, 0.0f));
                    }
        else if(5.0f + (30+countTour*40) - glfwGetTime()>=-5.0f){

            float ypos = 5.0f + (30+countTour*40) - glfwGetTime();
            model = glm::translate(model,glm::vec3(10.0f,0.0f,ypos));
            }
        

        GLfloat angleGiraf = M_PI ;
            model = glm::rotate(model, angleGiraf, glm::vec3(0.0f, 2.0f, 2.0f));

        model = glm::scale(model, glm::vec3(0.01f));
       
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        giraf.Draw(shaders);

        }


        // VBA is detached from the current object in the OpenGL context
        glBindVertexArray(0);

        // Exchange rendering buffers to update the window with new content
        glfwSwapBuffers(window);

    }



    // Delete the objects and buffer that we created earlier
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    // End of the program, we clean up the context created by the GLFW
    glfwTerminate();

    return 0;
}
