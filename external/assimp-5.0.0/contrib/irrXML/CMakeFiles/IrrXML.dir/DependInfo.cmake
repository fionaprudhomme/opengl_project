# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/contrib/irrXML/irrXML.cpp" "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/contrib/irrXML/CMakeFiles/IrrXML.dir/irrXML.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ASSIMP_BUILD_NO_C4D_IMPORTER"
  "ASSIMP_BUILD_NO_OWN_ZLIB"
  "GLEW_STATIC"
  "TW_NO_DIRECT3D"
  "TW_NO_LIB_PRAGMA"
  "TW_STATIC"
  "_CRT_SECURE_NO_WARNINGS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "external/assimp-5.0.0/include"
  "external/assimp-5.0.0"
  "external/assimp-5.0.0/code"
  "external/assimp-5.0.0/."
  "external/glfw-3.3/include/GLFW"
  "external/glew-2.1.0/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
