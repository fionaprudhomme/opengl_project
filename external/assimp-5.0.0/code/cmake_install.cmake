# Install script for directory: /home/formation/Bureau/opengl_project/external/assimp-5.0.0/code

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/formation/Bureau/opengl_project/lib/libassimp.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xassimp-devx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp" TYPE FILE FILES
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/anim.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/aabb.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/ai_assert.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/camera.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/color4.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/color4.inl"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/config.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/defs.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Defines.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/cfileio.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/light.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/material.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/material.inl"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/matrix3x3.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/matrix3x3.inl"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/matrix4x4.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/matrix4x4.inl"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/mesh.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/pbrmaterial.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/postprocess.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/quaternion.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/quaternion.inl"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/scene.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/metadata.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/texture.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/types.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/vector2.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/vector2.inl"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/vector3.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/vector3.inl"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/version.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/cimport.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/importerdesc.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Importer.hpp"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/DefaultLogger.hpp"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/ProgressHandler.hpp"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/IOStream.hpp"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/IOSystem.hpp"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Logger.hpp"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/LogStream.hpp"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/NullLogger.hpp"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/cexport.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Exporter.hpp"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/DefaultIOStream.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/DefaultIOSystem.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/ZipArchiveIOSystem.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/SceneCombiner.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/fast_atof.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/qnan.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/BaseImporter.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Hash.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/MemoryIOWrapper.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/ParsingUtils.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/StreamReader.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/StreamWriter.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/StringComparison.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/StringUtils.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/SGSpatialSort.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/GenericProperty.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/SpatialSort.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/SkeletonMeshBuilder.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/SmoothingGroups.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/SmoothingGroups.inl"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/StandardShapes.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/RemoveComments.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Subdivision.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Vertex.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/LineSplitter.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/TinyFormatter.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Profiler.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/LogAux.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Bitmap.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/XMLTools.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/IOStreamBuffer.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/CreateAnimMesh.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/irrXMLWrapper.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/BlobIOSystem.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/MathFunctions.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Macros.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Exceptional.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/ByteSwapper.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xassimp-devx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/assimp/Compiler" TYPE FILE FILES
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Compiler/pushpack1.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Compiler/poppack1.h"
    "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/../include/assimp/Compiler/pstdint.h"
    )
endif()

