# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/formation/Bureau/opengl_project/main.cpp" "/home/formation/Bureau/opengl_project/CMakeFiles/main.dir/main.cpp.o"
  "/home/formation/Bureau/opengl_project/src/Shader.cpp" "/home/formation/Bureau/opengl_project/CMakeFiles/main.dir/src/Shader.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  "TW_NO_DIRECT3D"
  "TW_NO_LIB_PRAGMA"
  "TW_STATIC"
  "_CRT_SECURE_NO_WARNINGS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "external/glfw-3.3/include/GLFW"
  "external/glfw-3.3/include"
  "external/glm-0.9.9.6"
  "external/glew-2.1.0/include"
  "external/soil/src"
  "external/assimp-5.0.0/include"
  "external/freeglut-3.2.1/include/GL"
  "include"
  "."
  "external/assimp-5.0.0/code/../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/formation/Bureau/opengl_project/external/glfw-3.3/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  "/home/formation/Bureau/opengl_project/external/CMakeFiles/GLEW_210.dir/DependInfo.cmake"
  "/home/formation/Bureau/opengl_project/external/CMakeFiles/SOIL.dir/DependInfo.cmake"
  "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/code/CMakeFiles/assimp.dir/DependInfo.cmake"
  "/home/formation/Bureau/opengl_project/external/assimp-5.0.0/contrib/irrXML/CMakeFiles/IrrXML.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
